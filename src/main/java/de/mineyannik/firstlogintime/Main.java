/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.mineyannik.firstlogintime;

import de.mineyannik.firstlogintime.command.FirstLogin;
import de.mineyannik.firstlogintime.listener.Login;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author yannikhenke
 */
public class Main extends JavaPlugin {
    
    @Override
    public void onEnable()
    {
        this.getCommand("firstlogin").setExecutor(new FirstLogin(this));
        this.getServer().getPluginManager().registerEvents(new Login(this), this);
    }
}
/*Changelog
    Code Optimations
*/
