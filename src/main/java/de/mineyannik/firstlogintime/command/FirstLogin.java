/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.mineyannik.firstlogintime.command;

import de.mineyannik.firstlogintime.Main;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;

/**
 *
 * @author yannikhenke
 */
public class FirstLogin implements CommandExecutor{

    private final Main plugin;
    
    public FirstLogin (Main plugin)
    {
        this.plugin = plugin;
    }
    
    @Override
    public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] args) {
        
        if (args.length == 1)
        {
            if (cs.hasPermission("firstlogintime.firstlogin"))
            {
                FileConfiguration config = plugin.getConfig();
                plugin.reloadConfig();
                
                if (config.contains(args[0]))
                {
                    cs.sendMessage(ChatColor.GREEN + "Player " + args[0] + " loged in at " + config.getString(args[0]));
                }
                else
                {
                    cs.sendMessage(ChatColor.RED + "The player " + args[0] + " is not saved.");
                }
                
                return true;
            }
            else
            {
                cs.sendMessage(ChatColor.DARK_RED + "You do not have enough Permissions to perform that command.");
                return true;
            }
        }
        
        return false;
    }
    
    
}
