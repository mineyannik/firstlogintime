/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.mineyannik.firstlogintime.listener;

import de.mineyannik.firstlogintime.Main;
import java.sql.Date;
import java.text.SimpleDateFormat;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 *
 * @author yannikhenke
 */
public class Login implements Listener {
    
    private Main plugin;
    
    public Login (Main plugin)
    {
        this.plugin = plugin;
    }
    
    @EventHandler
    public void onJoin(PlayerJoinEvent e)
    {
        if (!(e.getPlayer().hasPlayedBefore()))
        {
            this.plugin.reloadConfig();
            
            FileConfiguration config = this.plugin.getConfig();
            
            Date zeitstempel = new Date(System.currentTimeMillis());
            
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
            
            String datum = simpleDateFormat.format(zeitstempel);
            
            config.set(e.getPlayer().getName(), datum);
            
            this.plugin.saveConfig();
        }
    }
    
}
